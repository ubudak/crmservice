﻿using CrmService;
using Grpc.Net.Client;
using System;
using System.Threading.Tasks;

namespace CrmServiceClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //var httpHandler = new HttpClientHandler();
            //// Return `true` to allow certificates that are untrusted/invalid
            //httpHandler.ServerCertificateCustomValidationCallback =
            //    HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            //using var channel = GrpcChannel.ForAddress("http://localhost:5001",
            //    new GrpcChannelOptions { HttpHandler = httpHandler });

            //https kullanılırken kaldırılmalıdır
            AppContext.SetSwitch(
                "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            // The port number(5001) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("http://localhost:5001");

            //For making gRPC-Web calls
            //var channel = GrpcChannel.ForAddress("https://localhost:5001", new GrpcChannelOptions
            //{
            //    HttpHandler = new GrpcWebHandler(new HttpClientHandler())
            //});

            var client = new Product.ProductClient(channel);
            var reply = await client.GetProductAsync(
                new GetProductRequest {Id = 5});

            Console.WriteLine("GetProductReply: " + reply);
            Console.WriteLine("---");

            //Common.PrintMessage(reply);
            //Console.WriteLine("---");

            var client2 = new Store.StoreClient(channel);
            var reply2 = await client2.GetStoreAsync(
                new GetStoreRequest { Id = 8 });

            Console.WriteLine("GetStoreReply: " + reply2);
            Console.WriteLine("---");

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
