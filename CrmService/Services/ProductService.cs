﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CrmService.Services
{
    public class ProductService: Product.ProductBase
    {
        private readonly ILogger<ProductService> _logger;

        public ProductService(ILogger<ProductService> logger)
        {
            _logger = logger;
        }

        public override Task<GetProductReply> GetProduct(GetProductRequest request, ServerCallContext context)
        {
            return Task.FromResult(new GetProductReply
            {
                Id = request.Id,
                ProductName = "Taahhütlü 20 Emlak",
                ProductCode = 30
            });
        }
    }
}
