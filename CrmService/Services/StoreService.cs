﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CrmService.Services
{
    public class StoreService : Store.StoreBase
    {
        private readonly ILogger<StoreService> _logger;

        public StoreService(ILogger<StoreService> logger)
        {
            _logger = logger;
        }

        public override Task<GetStoreReply> GetStore(GetStoreRequest request, ServerCallContext context)
        {
            return Task.FromResult(new GetStoreReply
            {
                Id = request.Id,
                StoreName = "Umut Emlak",
                StoreNumber = 881317,
                StoreTenure = 4
            });
        }

        public override Task<GetStoreOwnerReply> GetStoreOwner(GetStoreRequest request, ServerCallContext context)
        {
            return Task.FromResult(new GetStoreOwnerReply
            {
                Id = request.Id,
                FullName = "John Doe",
                Age = 42
            });
        }
    }
}
